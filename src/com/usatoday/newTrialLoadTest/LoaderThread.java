package com.usatoday.newTrialLoadTest;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.joda.time.DateTime;

import com.usatoday.businessObjects.util.MD5OneWayHash;

public class LoaderThread implements Runnable {

	private String remoteServerURL = null;
	private String partnerID = null;
	private String partnerHash = null;
	
	private String statusString = "unused thread";

	private ArrayList<ArrayList<String>> data = null;
	
	public LoaderThread() {
		this.data = new ArrayList<ArrayList<String>>();
	}
	
	@Override
	public void run() {
		
		DateTime time = new DateTime();
		
		long threadID = Thread.currentThread().getId();
		StringBuilder statusBuilder = new StringBuilder(time.toString("hh:mm:ss") + " - Thread ID: " + threadID + " beginning processing of " + this.data.size() + " records.\n");
		
		int count = 0;
		int addedCount = 0;
		int existCount = 0;
		int failedCount = 0;
		String ip = null;
		try {
			java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
			ip = localMachine.getHostAddress();
		}
		catch (java.net.UnknownHostException uhe) { // [beware typo in code sample -dmw]
			ip = "10.204.1.96";
		}

			
		try {
			
			for (ArrayList<String> row : this.data) {
				count++;
				String firstName = null;
				String lastName = null;
				String email = null;
				
				if (count > 1 && ((count % 100) == 0)) {
					time = new DateTime();
					statusBuilder.append(time.toString("hh:mm:ss") + " - Thread ID: " + threadID + " Processed " + count + "records. \n" );
				}
				
				try {
					firstName = row.get(0);
					firstName = firstName.trim();
					if (firstName.length() > 10) {
						//if first name > 10 chars MUST abbreviate
						firstName = firstName.substring(0, 1);
						firstName = firstName + ".";
					}
					lastName = row.get(1);
					lastName = lastName.trim();
					if (lastName.length() > 15) {
						// if last name > 15 chars MUST abbreviate
						lastName = lastName.substring(0,1);
						lastName = lastName + ".";
					}
					email = row.get(2);
					email = email.trim();
					if (email.length() > 60) {
						failedCount++;
						time = new DateTime();
						statusBuilder.append(time.toString("hh:mm:ss") + " - Thread ID: " + threadID + " Error with input record - email address too big: " + count + "  First Name: " + firstName + "  Last Name: " + lastName + "  Email: " + email + "\n");
						continue;
					}
					
					if (firstName != null && lastName != null && email != null) {

						String myHash = null;
						
						URL url = new URL(this.getRemoteServerURL());
				        
				        java.net.HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
				        urlConnection.setDoInput(true);
				        urlConnection.setDoOutput(true);
				        urlConnection.setUseCaches(false);
				        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
				        
				        StringBuilder requestData = new StringBuilder();
				        requestData.append(URLEncoder.encode("partnerID","UTF-8")).append("=").append(URLEncoder.encode(this.getPartnerID(),"UTF-8"));
				        requestData.append("&").append(URLEncoder.encode("partnerIP","UTF-8")).append("=").append(URLEncoder.encode(ip,"UTF-8"));
				        requestData.append("&").append(URLEncoder.encode("firstName","UTF-8")).append("=").append(URLEncoder.encode(firstName,"UTF-8"));
				        requestData.append("&").append(URLEncoder.encode("lastName","UTF-8")).append("=").append(URLEncoder.encode(lastName, "UTF-8"));
				        // next line added as a work around for a bug in the trial add system
				        requestData.append("&").append(URLEncoder.encode("password","UTF-8")).append("=").append(URLEncoder.encode("", "UTF-8"));				        
				        requestData.append("&").append(URLEncoder.encode("emailAddress","UTF-8")).append("=").append(URLEncoder.encode(email,"UTF-8"));
				        
						if (this.partnerHash != null) {
							StringBuilder hashValue = new StringBuilder(this.getPartnerID());
							hashValue.append(ip);
							hashValue.append(firstName);
							hashValue.append(lastName);
							hashValue.append(email);
							hashValue.append("");
							hashValue.append(this.partnerHash);
							
							myHash = MD5OneWayHash.getMD5Hash(hashValue.toString());
							
							requestData.append("&").append(URLEncoder.encode("hash","UTF-8")).append("=").append(URLEncoder.encode(myHash,"UTF-8"));
						}
				        
				        
				        // requestData.append("&").append(URLEncoder.encode("debug","UTF-8")).append("=").append(URLEncoder.encode("Y","UTF-8"));
				        
				        //this.getProcessLog().finest(requestData.toString());
				        
				        DataOutputStream dOut = new DataOutputStream(urlConnection.getOutputStream());
				        dOut.writeBytes(requestData.toString());
				        dOut.flush();
				        dOut.close();
				        
				        BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
				        String line;
				        StringBuilder responseString = new StringBuilder();
				        while ((line = rd.readLine()) != null) {
				            // Process line...
				        	if (line.indexOf("<Added>Y</Added>") > -1) {
				        		addedCount++;
				        	}
				        	else if (line.indexOf("<Added>N</Added>") > -1) {
				        		failedCount++;
				        		if (line.indexOf("A trial account already exists") > -1) {
				        			existCount++;
				        		}
				        		else {
				        			statusBuilder.append("Thread ID: " + threadID + " Failed to process record with email: " + email + "\n");
				        		}
				        	}
				            responseString.append(line);
				        }
				        
				        rd.close();

						
					}
					else {
						time = new DateTime();
						statusBuilder.append(time.toString("hh:mm:ss") + " - Thread ID: " + threadID + " Error with input record: " + count + "  First Name: " + firstName + "  Last Name: " + lastName + "  Email: " + email + "\n");
					}
				}
				catch (Exception e) {
					//System.out.println(" Error with record: " + count + "  First Name: " + firstName + "  Last Name: " + lastName + "  Email: " + email + "  Exception: " + e.getMessage());
					time = new DateTime();
					statusBuilder.append(time.toString("hh:mm:ss") + " - Thread ID: " + threadID + " Error with record: " + count + "  First Name: " + firstName + "  Last Name: " + lastName + "  Email: " + email + "  Exception: " + e.getMessage() + " \n");
				}
			} // end for
			
		}
		catch (Exception e) {
			e.printStackTrace();
			//this.getProcessLog().severe(e.getMessage());
		}
		
		time = new DateTime();
		
		statusBuilder.append(time.toString("hh:mm:ss") + " - Thread ID: " + threadID + " Successfully Added Count: " + addedCount + "\n");
		statusBuilder.append(time.toString("hh:mm:ss") + " - Thread ID: " + threadID + " Failed to Add Count: " + failedCount + "\n");
		statusBuilder.append(time.toString("hh:mm:ss") + " - Thread ID: " + threadID + " Of Failed, already existing: " + existCount + "\n");
		
		this.statusString = statusBuilder.toString();

	}

	public String getRemoteServerURL() {
		return remoteServerURL;
	}

	public void setRemoteServerURL(String remoteServerURL) {
		this.remoteServerURL = remoteServerURL;
	}

	public String getPartnerID() {
		return partnerID;
	}

	public void setPartnerID(String partnerID) {
		this.partnerID = partnerID;
	}

	public String getPartnerHash() {
		return partnerHash;
	}

	public void setPartnerHash(String partnerHash) {
		this.partnerHash = partnerHash;
	}

	public String getStatusString() {
		return statusString;
	}

	public void addRowOfData(ArrayList<String> row) {
		this.data.add(row);
	}
	
	public int getNumberRows() {
		return this.data.size();
	}
}
