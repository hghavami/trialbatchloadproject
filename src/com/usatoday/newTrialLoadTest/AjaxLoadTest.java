package com.usatoday.newTrialLoadTest;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import org.joda.time.DateTime;


public class AjaxLoadTest implements Runnable {

		long timeToRun = 0;

	   private int numThreads = 0;
	   private int numIterations = 0;
	   private int startingEmailNumber = 5001;
	   
       private StringBuilder testResults = new StringBuilder();
       
       
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {

			if (args.length != 2) {
				System.out.println("usage: [Num iterations for each thread] [Number of threads]");
				System.exit(1);
			}
			
			AjaxLoadTest lt = new AjaxLoadTest();
			  
			lt.setNumIterations(Integer.parseInt(args[0]));
			lt.setNumThreads(Integer.parseInt(args[1]));
			   
			lt.runTest();
			
			System.out.println(lt.getTestResults());
	        
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	   public void runTest() {
	        ArrayList<Thread> threads = new ArrayList<Thread>();
	        ArrayList<AjaxLoadTest> trialLoadObjs = new ArrayList<AjaxLoadTest>();

	        int testSize = this.getNumThreads();
	        int startingEmail = this.startingEmailNumber;
	        
	        for (int i = 0; i < testSize; i++) {
	        	AjaxLoadTest lt = new AjaxLoadTest();

	        	lt.setNumIterations(this.getNumIterations());
	        	lt.setStartingEmailNumber(startingEmail);
	        	
	        	startingEmail += this.getNumIterations();
	        	
	            Thread t = new Thread(lt);

	            threads.add(t);
	            trialLoadObjs.add(lt);
	        }

	        DateTime testStart = new DateTime();
	        
	        // Start the threads
	        for (int j = 0; j < threads.size(); j++) {
	            Thread t = threads.get(j);
	            t.start();
	        }

	        // join the threads
	        for (int j = 0; j < threads.size(); j++) {
	            Thread t = threads.get(j);
	            try {
	                t.join();
	            } catch (Exception e) {
	                System.out.println("Thread run method interrupted.."
	                        + e.getMessage());
	            }
	        }
	        
	        DateTime end = new DateTime();
	        
	        long time = 0;

	        StringBuffer outputBuf = new StringBuffer();
	        Iterator<AjaxLoadTest> itr = trialLoadObjs.iterator();
	        while(itr.hasNext()) {
	        	AjaxLoadTest lt = itr.next();
	            
	            time += lt.getTimeToRun();
	            outputBuf.append("Time for thread: " + lt.getTimeToRun() + "  Results: \n");
	            outputBuf.append(lt.getTestResults() + "\n\n");
	            //System.out.println("Time for thread: " + lt.getTimeToRun());
	            
	        }
	        
	        outputBuf.append("\n");
	        //System.out.println();
	        //System.out.println("It took an average of:  " + (time / testSize));
	        outputBuf.append("It took an average of:  " + (time / (this.numIterations * this.numThreads)) + " for each addition");
	        this.testResults.append(outputBuf.toString());
	        long testTime = end.getMillis() - testStart.getMillis();
	        double seconds = testTime / 1000.0;
	        
	        this.testResults.append("\nTotal Test Run Time (seconds): " + seconds);
	    }
	
	@Override
	public void run() {
		
        try {
			// last start count:  
			
        	int startingEmailCount = this.startingEmailNumber;
        	
			DateTime startTime = new DateTime();
			
			for (int i = startingEmailCount; i < (startingEmailCount + this.numIterations); i++) {
		        URL url = new URL("http://usat-aeast-1/subscriptions/trials/trialAdd.do");
		        
		        java.net.HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
		        urlConnection.setDoInput(true);
		        urlConnection.setDoOutput(true);
		        urlConnection.setUseCaches(false);
		        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		        
		        StringBuilder data = new StringBuilder();
		        data.append(URLEncoder.encode("partnerID","UTF-8")).append("=").append(URLEncoder.encode("LOADTEST","UTF-8"));
		        data.append("&").append(URLEncoder.encode("partnerIP","UTF-8")).append("=").append(URLEncoder.encode("172.1.1.1","UTF-8"));
		        data.append("&").append(URLEncoder.encode("clubNumber","UTF-8")).append("=").append(URLEncoder.encode("","UTF-8"));
		        data.append("&").append(URLEncoder.encode("firstName","UTF-8")).append("=").append(URLEncoder.encode("Andy","UTF-8"));
		        data.append("&").append(URLEncoder.encode("lastName","UTF-8")).append("=").append(URLEncoder.encode("East", "UTF-8"));
		        data.append("&").append(URLEncoder.encode("address1","UTF-8")).append("=").append(URLEncoder.encode("17545 Brookville Court","UTF-8"));
		        data.append("&").append(URLEncoder.encode("apartmentSuite","UTF-8")).append("=").append(URLEncoder.encode("","UTF-8"));
		        data.append("&").append(URLEncoder.encode("city","UTF-8")).append("=").append(URLEncoder.encode("Round Hill","UTF-8"));
		        data.append("&").append(URLEncoder.encode("state","UTF-8")).append("=").append(URLEncoder.encode("VA","UTF-8"));
		        data.append("&").append(URLEncoder.encode("zip","UTF-8")).append("=").append(URLEncoder.encode("20141","UTF-8"));
		        data.append("&").append(URLEncoder.encode("phone","UTF-8")).append("=").append(URLEncoder.encode("7038545407","UTF-8"));
		        
		        String emailAddress = "andyLoadTest" + i + "@eastzilla.com";
		        
		        data.append("&").append(URLEncoder.encode("emailAddress","UTF-8")).append("=").append(URLEncoder.encode(emailAddress,"UTF-8"));
		        data.append("&").append(URLEncoder.encode("password","UTF-8")).append("=").append(URLEncoder.encode("apiPwd","UTF-8"));
		        data.append("&").append(URLEncoder.encode("debug","UTF-8")).append("=").append(URLEncoder.encode("Y","UTF-8"));
		        
		        
		        DataOutputStream dOut = new DataOutputStream(urlConnection.getOutputStream());
		        dOut.writeBytes(data.toString());
		        dOut.flush();
		        dOut.close();
		        
		        BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
		        String line;
		        StringBuilder responseString = new StringBuilder();
		        while ((line = rd.readLine()) != null) {
		            // Process line...
		            responseString.append(line);
		        }
		        
		        rd.close();

		        this.testResults.append(responseString.toString()).append("\n");
				
			}

			DateTime endTime = new DateTime();
			
			long milliseconds = endTime.getMillis() - startTime.getMillis();
			
			double seconds = milliseconds / 1000.0;
			
			this.testResults.append("\nDone " + this.numIterations + " iterations. Took: " + seconds + " seconds to complete.");
            
            
            this.timeToRun = (endTime.getMillis() - startTime.getMillis());
            
        } catch (Exception e) {
            e.printStackTrace();
        }
		
		
	}

	public int getNumThreads() {
		return numThreads;
	}

	public void setNumThreads(int numThreads) {
		this.numThreads = numThreads;
	}

	public long getTimeToRun() {
		return timeToRun;
	}

	public void setTimeToRun(long timeToRun) {
		this.timeToRun = timeToRun;
	}

	public int getNumIterations() {
		return numIterations;
	}

	public void setNumIterations(int numIterations) {
		this.numIterations = numIterations;
	}

	public int getStartingEmailNumber() {
		return startingEmailNumber;
	}

	public void setStartingEmailNumber(int startingEmailNumber) {
		this.startingEmailNumber = startingEmailNumber;
	}

	public String getTestResults() {
		return testResults.toString();
	}

	public void setTestResults(String testResults) {
		this.testResults = new StringBuilder(testResults);
	}

	
	
}
