package com.usatoday.newTrialLoadTest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class CSVDataLoader {

	private String inputFileName = null;
	
	private ArrayList<ArrayList<String>> data = null;

	public CSVDataLoader() {
		this.data = new ArrayList<ArrayList<String>>();
		
	}
	
	public String getInputFileName() {
		return inputFileName;
	}

	public void setInputFileName(String inputFileName) {
		this.inputFileName = inputFileName;
	}
	
	/**
	 * 
	 * @param clearOldData - if true, clears any data previously read in.
	 * @return number of rows in data array
	 * @throws Exception
	 */
	public int loadDataFromFile(boolean clearOldData) throws Exception {
	
		if (clearOldData) {
			for (ArrayList<String> row : this.data) {
				row.clear();
			}
			this.data.clear();
		}
		
		File f = new File(this.inputFileName);
		
		if (!f.exists()) {
			throw new Exception("Input File does not exist: " + this.inputFileName);
		}
		
		if (f.isDirectory()) {
			throw new Exception ("Specify the absolute path to the file to be read. " + this.inputFileName + " is a directory.");
		}
		
		if (f.length() == 0) {
			throw new Exception (this.inputFileName + " is an empty file.");
		}
		
		BufferedReader d = null;
		try {
			FileInputStream fis = new FileInputStream(f);
			InputStreamReader isr = new InputStreamReader(fis);
			d = new BufferedReader(isr);
			
			String aRow = d.readLine();
			while (aRow != null) {
				ArrayList<String> dataTokens = new ArrayList<String>();
				
				StringTokenizer tokenizer = new StringTokenizer(aRow, ",");
				
				while(tokenizer.hasMoreTokens()) {
					String token = tokenizer.nextToken();
					dataTokens.add(token.trim());
				}
				
				this.data.add(dataTokens);
				
				aRow = d.readLine();
			}

		}
		catch (Exception e) {
			throw e;
		}
		finally {
			if (d != null) {
				d.close();
			}
		}
		
		
		return this.data.size();
	}

	public ArrayList<ArrayList<String>> getData() {
		return data;
	}

	
}
