package com.usatoday.newTrialLoadTest;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.joda.time.DateTime;

import com.usatoday.businessObjects.util.MD5OneWayHash;

public class EETrialLoader {

	private String sourceDataFile = null;
	private CSVDataLoader dataLoader = null;
	private String remoteServerURL = null;
	private String partnerID = null;
	private String partnerHash = null;
	

	private Logger processLog = null;
	private Level  logLevel = Level.INFO;
	private String logFileDirName = ".";
	
	private boolean useMultipleThreads = true;
	private int maxThreads = 10;
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		 
		EETrialLoader loader = new EETrialLoader();

		loader.processCommandLineArgs(args);
		
		loader.initializeLogging();
		
		//loader.spitSourceDataToConsole();
		
		loader.loadDataToTrialSystem();
		
		loader.getProcessLog().info("EE Trial Load Terminating Normally.");
	}

	public EETrialLoader () {
		this.dataLoader = new CSVDataLoader();
	}

    protected void processCommandLineArgs(String[] args) {
        for (int i = 0; i < args.length; i++) {
            if (args[i].equalsIgnoreCase("-inputFile")) {
                int valueIndex = i + 1;
                if (valueIndex < args.length) {
                    String value = args[++i];
                    this.setSourceDataFile(value.trim());
                }
            }
            else if (args[i].equalsIgnoreCase("-logDir")) {
                int valueIndex = i + 1;
                if (valueIndex < args.length) {
                    String value = args[++i];
                    this.setLogFileDirName(value.trim());
                }
                else {
                	this.setLogFileDirName(".");
                }
            }
            else if (args[i].equalsIgnoreCase("-logLevel")) {
                int valueIndex = i + 1;
                if (valueIndex < args.length) {
                    String value = args[++i];
                    if (value.equalsIgnoreCase("FINE")) {
                    	this.logLevel = Level.FINE;
                    }
                    else if (value.equalsIgnoreCase("FINEST")) {
                    	this.logLevel = Level.FINEST;
                    }
                    else if (value.equalsIgnoreCase("INFO")) {
                    	this.logLevel = Level.INFO;
                    }
                    else if (value.equalsIgnoreCase("WARN")) {
                    	this.logLevel = Level.WARNING;
                    }
                    else if (value.equalsIgnoreCase("ALL")) {
                    	this.logLevel = Level.ALL;
                    }
                    
                }
                else {
                	this.setLogFileDirName(".");
                }
            }
            else if (args[i].equalsIgnoreCase("-destURL")) {
                int valueIndex = i + 1;
                if (valueIndex < args.length) {
                    String value = args[++i];
                    this.setRemoteServerURL(value.trim());
                }
            }
            else if (args[i].equalsIgnoreCase("-pid")) {
                int valueIndex = i + 1;
                if (valueIndex < args.length) {
                    String value = args[++i];
                    this.setPartnerID(value.trim());
                }
            }
            else if (args[i].equalsIgnoreCase("-hashKey")) {
                int valueIndex = i + 1;
                if (valueIndex < args.length) {
                    String value = args[++i];
                    this.setPartnerHash(value.trim());
                }
            }
            else if (args[i].equalsIgnoreCase("-multiThreaded")) {
                int valueIndex = i + 1;
                if (valueIndex < args.length) {
                    String value = args[++i];
                    if (value.equalsIgnoreCase("false")) {
                        this.useMultipleThreads = false;                    	
                    }
                }
            }
            else if (args[i].equalsIgnoreCase("-maxThreads")) {
                int valueIndex = i + 1;
                if (valueIndex < args.length) {
                	try {
                		String value = args[++i];
                		int numThreadOverride = Integer.valueOf(value);
                		this.maxThreads = numThreadOverride;
                	}
                	catch (Exception e) {
						e.printStackTrace();
					}
                }
            }
        } // end for 
        
        if (this.getSourceDataFile() == null || this.getSourceDataFile().trim().length() == 0) {
        	System.out.println("USEAGE: java -jar trialBatchLoad.jar -inputFile [full path to input file] -destURL [URL of Trial Loading Endpoint] -pid [Partner ID] -hashKey [Partner Secret Hash Key] -logDir [LOG File Directory] -logLevel [ALL|FINEST|FINE|INFO|WARN]");
        }
    }
	
    /**
     * Method used for testing input data
     */
	public void spitSourceDataToConsole() {
		
		this.dataLoader.setInputFileName(this.getSourceDataFile());
		
		try {
			int rowsRead  = this.dataLoader.loadDataFromFile(true);
			
			System.out.println(rowsRead + " Rows of Data Read into memory");
			
			ArrayList<ArrayList<String>> data =  this.dataLoader.getData();
			
			int count = 1;
			for (ArrayList<String> row : data) {
				StringBuilder sb = new StringBuilder();
				sb.append(count).append(":  ");
				for (String token : row) {
					sb.append("'").append(token).append("' ");
				}
				System.out.println(sb.toString());
				count++;
			}
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
				
	}

	/**
	 * Loads data to trial system
	 */
	private void loadDataToTrialSystem() {
		
		this.getProcessLog().info("Processing Input File: " + this.getSourceDataFile());
		
		String ip = null;
		try {
			java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
			ip = localMachine.getHostAddress();
		}
		catch (java.net.UnknownHostException uhe) { // [beware typo in code sample -dmw]
			ip = "10.204.1.96";
		}
		
		DateTime startTime = new DateTime();

		this.getProcessLog().info("Processing Start Time: " + startTime.toString("MM/dd/yyyy hh:mm:ss"));

		this.dataLoader.setInputFileName(this.getSourceDataFile());
		
		int count = 1;
		int rowsRead = 0;
		try {
			rowsRead  = this.dataLoader.loadDataFromFile(true);
			
			this.getProcessLog().info(rowsRead + " Rows of Data Read into memory");
			
			ArrayList<ArrayList<String>> data =  this.dataLoader.getData();
			
			
			if (this.useMultipleThreads && rowsRead > this.maxThreads) {
				this.getProcessLog().info("Using multiple threads of execution.");
				
				int recordsPerThread = (rowsRead / this.maxThreads) + 1;
				
				this.getProcessLog().info(recordsPerThread + " maximum rows per thread.");
				
				ArrayList<Thread> processingThreads = new ArrayList<Thread>();
				ArrayList<LoaderThread> loaders = new ArrayList<LoaderThread>();
				
				// ititialize threads
				for (int i = 0; i < this.maxThreads; i++) {
					LoaderThread threadLoader = new LoaderThread();
					threadLoader.setPartnerHash(this.partnerHash);
					threadLoader.setPartnerID(this.partnerID);
					threadLoader.setRemoteServerURL(this.getRemoteServerURL());
					Thread aThread = new Thread(threadLoader);
					processingThreads.add(aThread);
					loaders.add(threadLoader);
				}
				
				this.getProcessLog().info("Distributing work across threads.....");				
				// load threas with data
				int iteration= 0;
				for (ArrayList<String> row : data) {

					int threadIndex = iteration % this.maxThreads;
					
					
					LoaderThread lt = loaders.get(threadIndex);
					
					lt.addRowOfData(row);
					
					iteration++;
					
				}
				
				this.getProcessLog().info("Starting worker threads.....");				
				// start threads
				for (Thread t : processingThreads) {
					t.start();
				}
				
				this.getProcessLog().info("Waiting for worker threads to complete.....");				
				// wait for threads to finish
				for (Thread t : processingThreads) {
		           try {
		                t.join();
		            } catch (Exception e) {
		                System.out.println("Thread run method interrupted.."
		                        + e.getMessage());
		            }				
				}
				
				this.getProcessLog().info("Worker threads finished.");
				
				for (LoaderThread loader : loaders) {
					this.getProcessLog().info(loader.getStatusString());
				}
				
			}
			else {
				this.getProcessLog().info("Single Thread of Execution");
				String firstName = null;
				String lastName = null;
				String email = null;

				for (ArrayList<String> row : data) {
					
					try {
						firstName = row.get(0);
						firstName = firstName.trim();
						if (firstName.length() > 10) {
							//if first name > 10 chars MUST abbreviate
							firstName = firstName.substring(0, 1);
							firstName = firstName + ".";
						}
						lastName = row.get(1);
						lastName = lastName.trim();
						if (lastName.length() > 15) {
							// if last name > 15 chars MUST abbreviate
							lastName = lastName.substring(0,1);
							lastName = lastName + ".";
						}
						email = row.get(2);
						email = email.trim();
						if (email.length() > 60) {
							this.getProcessLog().warning("Error with input record - email address too big: " + count + "  First Name: " + firstName + "  Last Name: " + lastName + "  Email: " + email + "\n");
							continue;
						}
						
						if (firstName != null && lastName != null && email != null) {

							String myHash = null;
							
							URL url = new URL(this.getRemoteServerURL());
					        
					        java.net.HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
					        urlConnection.setDoInput(true);
					        urlConnection.setDoOutput(true);
					        urlConnection.setUseCaches(false);
					        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
					        
					        StringBuilder requestData = new StringBuilder();
					        requestData.append(URLEncoder.encode("partnerID","UTF-8")).append("=").append(URLEncoder.encode(this.getPartnerID(),"UTF-8"));
					        requestData.append("&").append(URLEncoder.encode("partnerIP","UTF-8")).append("=").append(URLEncoder.encode(ip,"UTF-8"));
					        requestData.append("&").append(URLEncoder.encode("firstName","UTF-8")).append("=").append(URLEncoder.encode(firstName,"UTF-8"));
					        requestData.append("&").append(URLEncoder.encode("lastName","UTF-8")).append("=").append(URLEncoder.encode(lastName, "UTF-8"));
					        // next line added as a work around for a bug in the trial add system
					        requestData.append("&").append(URLEncoder.encode("password","UTF-8")).append("=").append(URLEncoder.encode("", "UTF-8"));				        
					        requestData.append("&").append(URLEncoder.encode("emailAddress","UTF-8")).append("=").append(URLEncoder.encode(email,"UTF-8"));
					        
							if (this.partnerHash != null) {
								StringBuilder hashValue = new StringBuilder(this.getPartnerID());
								hashValue.append(ip);
								hashValue.append(firstName);
								hashValue.append(lastName);
								hashValue.append(email);
								hashValue.append("");
								hashValue.append(this.partnerHash);
								
								myHash = MD5OneWayHash.getMD5Hash(hashValue.toString());
								
								requestData.append("&").append(URLEncoder.encode("hash","UTF-8")).append("=").append(URLEncoder.encode(myHash,"UTF-8"));
							}
					        
					        
					        
					        this.getProcessLog().finest(requestData.toString());
					        
					        DataOutputStream dOut = new DataOutputStream(urlConnection.getOutputStream());
					        dOut.writeBytes(requestData.toString());
					        dOut.flush();
					        dOut.close();
					        
					        BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
					        String line;
					        StringBuilder responseString = new StringBuilder();
					        while ((line = rd.readLine()) != null) {
					            // Process line...
					            responseString.append(line);
					        }
					        
					        rd.close();

					        this.getProcessLog().finest(responseString.toString());
							
						}
						else {
							this.getProcessLog().severe("Error with record: " + count + "  First Name: " + firstName + "  Last Name: " + lastName + "  Email: " + email);
						}
					}
					catch (Exception e) {
						this.getProcessLog().severe("Error with record: " + count + "  First Name: " + firstName + "  Last Name: " + lastName + "  Email: " + email + "  Exception: " + e.getMessage());
					}
					count++;
				}
				
			} // end else don't use threads.
			
		}
		catch (Exception e) {
			e.printStackTrace();
			this.getProcessLog().severe(e.getMessage());
		}


		DateTime endTime = new DateTime();

		this.getProcessLog().info("Processing End Time: " + endTime.toString("MM/dd/yyyy hh:mm:ss"));
		
		long milliseconds = endTime.getMillis() - startTime.getMillis();
		
		double seconds = milliseconds / 1000.0;		
		
		this.getProcessLog().info("It took " + seconds + " seconds to complete procesing of " + rowsRead + " records.");
	}
	
	public String getSourceDataFile() {
		return sourceDataFile;
	}

	public void setSourceDataFile(String sourceDataFile) {
		this.sourceDataFile = sourceDataFile;
	}

	/**
	 * 
	 */
    public void initializeLogging() {
        try {
            if (this.getLogFileDirName() != null && this.getLogFileDirName().length() > 0) {
                
            }
            else {
                this.setLogFileDirName(".");
            }
            
            this.processLog = Logger.getLogger("com.usatoday.batch.EETrialLoader");

            this.processLog.setLevel(this.logLevel);
            
            this.processLog.addHandler(new java.util.logging.ConsoleHandler());
            
            DateTime start = new DateTime();
	        
            StringBuilder logFileName = new StringBuilder(this.getLogFileDirName()+ File.separator + "EETrialLoader_" + start.toString("MM_dd_yyyy") + ".log");
            
	        FileHandler f = new FileHandler(logFileName.toString(), true);
	        f.setFormatter(new java.util.logging.SimpleFormatter());
            this.processLog.addHandler(f);
            
            this.processLog.log(Level.CONFIG, "Eedition Trial Loader Batch Processor Initializing...");

            this.processLog.log(Level.CONFIG, "Log Level: " + this.logLevel.toString());

        }
        catch (Exception e) {
            if (this.processLog == null) {
                this.processLog = Logger.getAnonymousLogger();
                this.processLog.addHandler(new java.util.logging.ConsoleHandler());
            }
            this.processLog.severe("Failed to initialize logging for Eedition Trial Access Batch Processor! " + e.getMessage());
        }
    }

	public Logger getProcessLog() {
		return processLog;
	}

	public void setProcessLog(Logger processLog) {
		this.processLog = processLog;
	}

	public Level getLogLevel() {
		return logLevel;
	}

	public void setLogLevel(Level logLevel) {
		this.logLevel = logLevel;
	}

	public String getLogFileDirName() {
		return logFileDirName;
	}

	public void setLogFileDirName(String logFileDirName) {
		this.logFileDirName = logFileDirName;
	}

	public String getRemoteServerURL() {
		return remoteServerURL;
	}

	public void setRemoteServerURL(String remoteServerURL) {
		this.remoteServerURL = remoteServerURL;
	}

	public String getPartnerID() {
		return partnerID;
	}

	public void setPartnerID(String partnerID) {
		this.partnerID = partnerID;
	}

	public String getPartnerHash() {
		return partnerHash;
	}

	public void setPartnerHash(String partnerHash) {
		this.partnerHash = partnerHash;
	}

}

